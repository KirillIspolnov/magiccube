package org.kirillbogatikov.pkipt.cubes_magic;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static Cube INITIAL = new Cube();
    
    static {
        INITIAL.add(0).add(1).add(2).add(3).add(4).add(5);
    }
    
    public static void main(String[] args) {
        ArrayList<Cube> cubes = new ArrayList<Cube>();
        cubes.add(INITIAL);
        
        Scanner scanner = new Scanner(System.in);
        int min = scanner.nextInt();
        int max = scanner.nextInt();
        
        int missing;
        Cube cube;
        for(int i = min; i < max; i++) {
            missing = findMissing(cubes, i);
            if(missing != -1) {
                cube = cubes.get(cubes.size() - 1);
                if(cube.isFull()) {
                    cube = new Cube();
                    cubes.add(cube);
                }
                
                cube.add(missing);
            }
        }
        
        StringBuilder builder = new StringBuilder();
        for(Cube _cube : cubes) {
            builder.append(_cube.toReadableString()).append("\n");
        }
        
        System.out.println(builder);
        
        scanner.close();
    }

    public static String toString(ArrayList<Cube> cubes) {
        StringBuilder builder = new StringBuilder();
        for(Cube cube : cubes) {
            builder.append(cube);
        }
        return builder.append(" ").toString();
    }
    
    public static int findMissing(ArrayList<Cube> cubes, int number) {
        String number_string = String.valueOf(number);
        String cubes_string = toString(cubes);
        String digit;
        
        for(int i = 0; i < number_string.length(); i++) {
            digit = number_string.charAt(i) + "";
            if(!cubes_string.contains(digit))
                return Integer.parseInt(digit);
            
            cubes_string = cubes_string.replaceFirst(digit, "");
        }
        
        return -1;
    }
}
