package org.kirillbogatikov.pkipt.cubes_magic;

import java.util.Arrays;

public class Cube {
    private Integer[] sides;
    private int index;
    
    public Cube() {
        sides = new Integer[]{ -1, -1, -1, -1, -1, -1 };
        index = 0;
    }
    
    public Cube add(int i) {
        sides[index++] = i;
        return this;
    }
    
    public boolean isFull() {
        return index == 6;
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(int n : sides)
            builder.append(n);
        return builder.toString();
    }
    
    public String toReadableString() {
        return Arrays.toString(sides);
    }
}
